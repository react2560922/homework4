import { useSelector } from 'react-redux'
import ModalWindow from './ModalWindow'
import ProductsCard from './ProductCard'
import { getModal } from '../Slice/modalSlice'
import { useState } from 'react'

function ProductsList({ products, id }) {
  const [modalProduct, setModalProduct] = useState(null)

  const { isOpen, typeModal } = useSelector(getModal)

  return (
    <div className="bestsellers-content conteiner">
      {isOpen && typeModal == 'delete' && (
        <ModalWindow type="delete" modalProduct={modalProduct} />
      )}
      {isOpen && typeModal == 'add' && (
        <ModalWindow type="add" modalProduct={modalProduct} />
      )}
      <ul className="bestsellers-content__list">
        {products.map((productItem) => (
          <li key={productItem.sku} className="bestsellers-content__item">
            <ProductsCard
              productItem={productItem}
              setModalProduct={setModalProduct}
              id={id}
            />
          </li>
        ))}
      </ul>
    </div>
  )
}

export default ProductsList
