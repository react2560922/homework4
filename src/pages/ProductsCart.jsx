import '../style.scss'
import { useSelector } from 'react-redux'
import { selectCart } from '../Slice/cartSlice'
import ProductsList from '../components/ProductList'

function ProductsCart({ id }) {
  const cartCounter = useSelector(selectCart)

  return (
    <>
      {cartCounter.length === 0 ? (
        <p className="modal__body-text">Товар відсутній</p>
      ) : (
        <ProductsList products={cartCounter} id={id} />
      )}
    </>
  )
}

export default ProductsCart
